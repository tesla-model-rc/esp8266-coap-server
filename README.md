# Application

Bridging between ESP8266 and mobile application is done through the creation of an access point on the ESP8266. Furthermore, the ESP8266 opens a server socket through CoAP allowing other CoAP clients to transmit or receive data from a created resource (/topics). The [mobile application](https://gitlab.com/tesla-model-rc/mobile-app-CoAP-client) serves as the CoAP client and currently performs:

- (GET) `/heartbeat` will receive string: "tmrc:heartbeat"
- (GET) `/getRadars` will receive string: "aa,bb,cc,dd" where a is front radar, b is left radar, c is right radar, and d is back radar
- (PUT) `/setStatus` will send string - either "$stop\n" or "$start\n"
- (PUT) `/setCoordinates` will send coordinates as string - i.e. "#Sxxx.xxxxxx,Syyy.yyyyyy\n" where S is the sign and x is latitude and y is longitude

`/getRadars` resource will update through UART from a microcontroller.  The string must follow the format: "^%02,%02,%02,%02" where '^' is the identifier for setting this resource.  This length is also fixed where each radar data must have a width of no more than 2.

`/setStatus` resource has an identifier of '$', while `/setCoordinates` resource has an identifier of '#'.  Both resources require appending a new line character at the end of string.  This is essential for parsing strings on microcontroller side as the ESP8266 echoes received data over UART.

## Project Setup

This project is built with [ESP8266 RTOS SDK](https://github.com/espressif/ESP8266_RTOS_SDK) for programming the ESP8266.  FreeRTOS, UART, Wifi with CoAP libraries are fully utilized in this project.

See the Getting Started Guide for full steps to configure and use ESP-IDF to build projects.

## softAP - Wifi Access Point

ESP8266 opens an access point for other wireless devices to connect too.  This access point follows adaptation of one of this [example](https://github.com/espressif/ESP8266_RTOS_SDK/tree/master/examples/wifi/getting_started/softAP).

## UART Configuration

UART is simply transmitted over UART0 TX/RX pins.  This [example](https://github.com/espressif/ESP8266_RTOS_SDK/tree/master/examples/peripherals/uart_events) is integrated.

## CoAP Server

CoAP server would startup a daemon task, create resources and receive data from CoAP clients and transmit data to CoAP clients. This server follows adaptation of a couple examples:

- https://github.com/espressif/ESP8266_RTOS_SDK/tree/master/examples/protocols/coap_server
- https://github.com/obgm/libcoap-minimal/blob/master/server.cc
- https://github.com/obgm/libcoap/blob/develop/examples/coap-server.c

The Constrained Application Protocol (CoAP) is a specialized web transfer protocol for use with constrained nodes and constrained networks in the Internet of Things.
The protocol is designed for machine-to-machine (M2M) applications such as smart energy and building automation.

Please refer to [RFC7252](https://www.rfc-editor.org/rfc/pdfrfc/rfc7252.txt.pdf) for more details.

### Configure the project

```
make menuconfig
```

* Set `Default serial port` under `Serial Flasher config`

### Build and Flash

Build the project and erase the flash, then flash it to the board:

```
make erase_flash flash -j4
```

### Debugging

```
make monitor
```
