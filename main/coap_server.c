#include "coap_server.h"

#include <string.h>
#include <sys/socket.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_common.h"

#include "esp_system.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_netif.h"

#include "uart_events.h"

#include "coap.h"

#define COAP_DEFAULT_TIME_SEC 5
#define COAP_DEFAULT_TIME_USEC 0

// Define asynchronous states for resources
static coap_async_state_t * heartbeat_async;
static coap_async_state_t * get_radars_async;
static coap_async_state_t * set_status_async_state;
static coap_async_state_t * set_coordinates_async_state;

#define RADARS_MSG_EXPECTED_LENGTH 12U
#define RADARS_MSG_ID '^'

static char uart_rx_buffer[32];
static char radars_buffer[32] = "aa,bb,cc,dd";
static uint8_t uart_buffer_ctr;

void set_radars_buffer(char * radar_string, const size_t radar_string_size) {
  strncpy(uart_rx_buffer + uart_buffer_ctr, radar_string, radar_string_size);
  uart_buffer_ctr += radar_string_size;

  if (strlen(uart_rx_buffer) >= RADARS_MSG_EXPECTED_LENGTH) {
    if (uart_rx_buffer[0] == RADARS_MSG_ID) {
      strncpy(radars_buffer, uart_rx_buffer + 1, strlen(uart_rx_buffer) - 1);
    }
    memset(uart_rx_buffer, 0, sizeof(uart_rx_buffer));
    uart_buffer_ctr = 0;
  }
}

static void heartbeat_async_handler(coap_context_t * ctx, struct coap_resource_t * resource,
                                    const coap_endpoint_t * local_interface, coap_address_t * peer,
                                    coap_pdu_t * request, str * token, coap_pdu_t * response) {
  (void)resource;
  (void)token;

  heartbeat_async = coap_register_async(ctx, peer, request, COAP_ASYNC_SEPARATE | COAP_ASYNC_CONFIRM, (void *)"no data");

  // Construct CON message with heartbeat data
  unsigned char buf[3];
  const char * response_data = "tmrc:heartbeat";
  size_t size = sizeof(coap_hdr_t) + 20;
  response = coap_pdu_init(heartbeat_async->flags & COAP_MESSAGE_CON, COAP_RESPONSE_CODE(205), 0, size);
  response->hdr->id = coap_new_message_id(ctx);
  if (heartbeat_async->tokenlen) {
    coap_add_token(response, heartbeat_async->tokenlen, heartbeat_async->token);
  }
  coap_add_option(response, COAP_OPTION_CONTENT_TYPE, coap_encode_var_bytes(buf, COAP_MEDIATYPE_TEXT_PLAIN), buf);
  coap_add_data(response, strlen(response_data), (unsigned char*)response_data);

  if (coap_send(ctx, local_interface, &heartbeat_async->peer, response) == COAP_INVALID_TID) {

  }
  coap_delete_pdu(response);
  coap_async_state_t * tmp;
  coap_remove_async(ctx, heartbeat_async->id, &tmp);
  coap_free_async(heartbeat_async);
  heartbeat_async = NULL;
}

static void get_radars_async_handler(coap_context_t * ctx, struct coap_resource_t * resource,
                                     const coap_endpoint_t * local_interface, coap_address_t * peer,
                                     coap_pdu_t * request, str * token, coap_pdu_t * response) {
  (void)resource;
  (void)token;

  get_radars_async = coap_register_async(ctx, peer, request, COAP_ASYNC_SEPARATE | COAP_ASYNC_CONFIRM, (void *)"no data");

  // Construct CON message with heartbeat data
  unsigned char buf[3];
  size_t size = sizeof(coap_hdr_t) + 20;
  response = coap_pdu_init(get_radars_async->flags & COAP_MESSAGE_CON, COAP_RESPONSE_CODE(205), 0, size);
  response->hdr->id = coap_new_message_id(ctx);
  if (get_radars_async->tokenlen) {
    coap_add_token(response, get_radars_async->tokenlen, get_radars_async->token);
  }
  coap_add_option(response, COAP_OPTION_CONTENT_TYPE, coap_encode_var_bytes(buf, COAP_MEDIATYPE_TEXT_PLAIN), buf);
  coap_add_data(response, strlen(radars_buffer), (unsigned char *)radars_buffer);

  if (coap_send(ctx, local_interface, &get_radars_async->peer, response) == COAP_INVALID_TID) {
  }
  coap_delete_pdu(response);
  coap_async_state_t * tmp;
  coap_remove_async(ctx, get_radars_async->id, &tmp);
  coap_free_async(get_radars_async);
  get_radars_async = NULL;
}

static void set_status_async_handler(coap_context_t * ctx, struct coap_resource_t * resource,
                                     const coap_endpoint_t * local_interface, coap_address_t * peer,
                                     coap_pdu_t * request, str * token, coap_pdu_t * response) {
  (void)resource;
  (void)token;

  set_status_async_state = coap_register_async(ctx, peer, request, COAP_ASYNC_SEPARATE | COAP_ASYNC_CONFIRM, (void *)"no data");

  // Record message into data buffer and echo out
  size_t len;
  unsigned char * data;
  coap_get_data(request, &len, &data);
  char data_buffer[32] = { '\0' };
  // '$' defines setStatus identifier
  data_buffer[0] = '$';
  memcpy(data_buffer+1, data, len);
  // append new line character
  data_buffer[strlen(data_buffer)] = '\n';

  ESP_LOGI(esp_common__get_TAG(), "ECHO status: %s", data_buffer);
  // echo status over uart
  uart_events__write_line(data_buffer, strlen(data_buffer));

  // Construct ACK message
  unsigned char buf[3];
  size_t size = sizeof(coap_hdr_t) + 20;
  const char * response_data = "status rcv";
  response = coap_pdu_init(set_status_async_state->flags & COAP_MESSAGE_ACK, COAP_RESPONSE_CODE(205), 0, size);
  response->hdr->id = coap_new_message_id(ctx);
  if (set_status_async_state->tokenlen) {
    coap_add_token(response, set_status_async_state->tokenlen, set_status_async_state->token);
  }
  coap_add_option(response, COAP_OPTION_CONTENT_TYPE, coap_encode_var_bytes(buf, COAP_MEDIATYPE_TEXT_PLAIN), buf);
  coap_add_data(response, strlen(response_data), (unsigned char*)response_data);

  if (coap_send(ctx, local_interface, &set_status_async_state->peer, response) == COAP_INVALID_TID) {

  }
  coap_delete_pdu(response);
  coap_async_state_t * tmp;
  coap_remove_async(ctx, set_status_async_state->id, &tmp);
  coap_free_async(set_status_async_state);
  set_status_async_state = NULL;
}

static void set_coordinates_async_handler(coap_context_t * ctx, struct coap_resource_t * resource,
                                          const coap_endpoint_t * local_interface, coap_address_t * peer,
                                          coap_pdu_t * request, str * token, coap_pdu_t * response) {
  (void)resource;
  (void)token;

  set_coordinates_async_state = coap_register_async(ctx, peer, request, COAP_ASYNC_SEPARATE | COAP_ASYNC_CONFIRM, (void*)"no data");

  // Record message into data buffer and echo out
  size_t len;
  unsigned char * data;
  coap_get_data(request, &len, &data);
  char data_buffer[32] = { '\0' };
  // '#' defines setCoordinate identifier
  data_buffer[0] = '#';
  memcpy(data_buffer+1, data, len);
  // append new line character
  data_buffer[strlen(data_buffer)] = '\n';

  ESP_LOGI(esp_common__get_TAG(), "ECHO coordinates: %s", data_buffer);

  // echo coordinates over uart
  uart_events__write_line(data_buffer, strlen(data_buffer));

  // Construct ACK message
  unsigned char buf[3];
  size_t size = sizeof(coap_hdr_t) + 20;
  const char * response_data = "coord. rcv";
  response = coap_pdu_init(set_coordinates_async_state->flags & COAP_MESSAGE_ACK, COAP_RESPONSE_CODE(205), 0, size);
  response->hdr->id = coap_new_message_id(ctx);
  if (set_coordinates_async_state->tokenlen) {
    coap_add_token(response, set_coordinates_async_state->tokenlen, set_coordinates_async_state->token);
  }
  coap_add_option(response, COAP_OPTION_CONTENT_TYPE, coap_encode_var_bytes(buf, COAP_MEDIATYPE_TEXT_PLAIN), buf);
  coap_add_data(response, strlen(response_data), (unsigned char*)response_data);
  
  if (coap_send(ctx, local_interface, &set_coordinates_async_state->peer, response) == COAP_INVALID_TID) {

  }
  coap_delete_pdu(response);
  coap_async_state_t * tmp;
  coap_remove_async(ctx, set_coordinates_async_state->id, &tmp);
  coap_free_async(set_coordinates_async_state);
  set_coordinates_async_state = NULL;
}

void vCoAP_server__task(void * p) {
  (void)p;

  coap_context_t * ctx = NULL;
  coap_address_t serv_addr;

  coap_resource_t * heartbeat_resource = NULL;

  coap_resource_t * get_radars_resource = NULL;

  coap_resource_t * set_status_resource = NULL;
  coap_resource_t * set_coordinates_resource = NULL;

  fd_set readfds;
  struct timeval tv;
  int flags = 0;

  while (1) {
    // Prepare the CoAP server socket
    coap_address_init(&serv_addr);
    serv_addr.addr.sin.sin_family = AF_INET;
    serv_addr.addr.sin.sin_addr.s_addr = INADDR_ANY;
    serv_addr.addr.sin.sin_port = htons(COAP_DEFAULT_PORT);
    ctx = coap_new_context(&serv_addr);
    if (ctx) {
      flags = fcntl(ctx->sockfd, F_GETFL, 0);
      fcntl(ctx->sockfd, F_SETFL, flags | O_NONBLOCK);

      tv.tv_usec = COAP_DEFAULT_TIME_USEC;
      tv.tv_sec = COAP_DEFAULT_TIME_SEC;

      // Initialize resources
      heartbeat_resource = coap_resource_init((unsigned char*)"heartbeat", 9, 0);

      get_radars_resource = coap_resource_init((unsigned char*)"getRadars", 9, 0);

      set_status_resource = coap_resource_init((unsigned char*)"setStatus", 9, 0);
      set_coordinates_resource = coap_resource_init((unsigned char*)"setCoordinates", 14, 0);

      coap_register_handler(heartbeat_resource, COAP_REQUEST_GET, heartbeat_async_handler);

      coap_register_handler(get_radars_resource, COAP_REQUEST_GET, get_radars_async_handler);

      coap_register_handler(set_status_resource, COAP_REQUEST_PUT, set_status_async_handler);
      coap_register_handler(set_coordinates_resource, COAP_REQUEST_PUT, set_coordinates_async_handler);

      coap_add_resource(ctx, heartbeat_resource);
      coap_add_resource(ctx, get_radars_resource);
      coap_add_resource(ctx, set_status_resource);
      coap_add_resource(ctx, set_coordinates_resource);

      // For incoming connections
      for (;;) {
        FD_ZERO(&readfds);
        FD_CLR(ctx->sockfd, &readfds);
        FD_SET(ctx->sockfd, &readfds);

        int result = select(ctx->sockfd + 1, &readfds, 0, 0, &tv);
        if (result > 0) {
          if (FD_ISSET(ctx->sockfd, &readfds)) coap_read(ctx);
        } else if (result < 0) {
          break;
        } else {
          ESP_LOGW(esp_common__get_TAG(), "select timeout");
        }
      }

      coap_free_context(ctx);
    }
  }

  vTaskDelete(NULL);
}
