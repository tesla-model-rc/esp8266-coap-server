#pragma once

#include <stddef.h>

void vCoAP_server__task(void * p);

void set_radars_buffer(char * radar_string, const size_t radar_string_size);
