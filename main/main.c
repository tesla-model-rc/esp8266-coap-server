#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_log.h"
#include "nvs_flash.h"

#include "esp_common.h"
#include "wifi_configuration.h"
#include "coap_server.h"
#include "uart_events.h"

void app_main() {
    ESP_ERROR_CHECK(nvs_flash_init());
    ESP_LOGI(esp_common__get_TAG(), "ESP_WIFI_MODE_AP");

    wifi_configuration__init_softap();
    uart_events__init();

    xTaskCreate(vCoAP_server__task, "coap_server", 4096, NULL, 5, NULL);
    xTaskCreate(vUART_events__task, "uart_event_task", 2048, NULL, 4, NULL);
}
