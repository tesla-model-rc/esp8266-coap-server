#include "stddef.h"

// Initializes UART0 
void uart_events__init(void);
// Task ISR handler  for UART event
void vUART_events__task(void *pvParameters);
// UART write lines that have appended a newline control character
void uart_events__write_line(const char * buffer, size_t buffer_size);
