#include "wifi_configuration.h"

#include "esp_common.h"

#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h"

#define ESP8266_WIFI_SSID    "tesla_model_rc"
#define ESP8266_WIFI_PASS    "passpass"
#define ESP8266_MAX_STA_CONN  4

static void wifi_configuration__event_handler(void * arg, esp_event_base_t event_base,
                                              int32_t event_id, void * event_data) {
  if (event_id == WIFI_EVENT_AP_STACONNECTED) {
    wifi_event_ap_staconnected_t * event = (wifi_event_ap_staconnected_t *)event_data;
    ESP_LOGI(esp_common__get_TAG(), "station "MACSTR" join, AID=%d",
      MAC2STR(event->mac), event->aid);
  }
  else if (event_id == WIFI_EVENT_AP_STADISCONNECTED) {
    wifi_event_ap_stadisconnected_t * event = (wifi_event_ap_stadisconnected_t *)event_data;
    ESP_LOGI(esp_common__get_TAG(), "station "MACSTR" leave, AID=%d",
      MAC2STR(event->mac), event->aid);
  }
}

void wifi_configuration__init_softap(void) {
  tcpip_adapter_init();
  ESP_ERROR_CHECK(esp_event_loop_create_default());

  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK(esp_wifi_init(&cfg));

  ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &wifi_configuration__event_handler, NULL));

  wifi_config_t ap_wifi_config = {
    .ap = {
      .ssid = ESP8266_WIFI_SSID,
      .ssid_len = strlen(ESP8266_WIFI_SSID),
      .password = ESP8266_WIFI_PASS,
      .max_connection = ESP8266_MAX_STA_CONN,
      .authmode = WIFI_AUTH_WPA_WPA2_PSK // NOTE: shortest password allowed for WPA2 is 8 characters
    }
  };

  if (strlen(ESP8266_WIFI_PASS) == 0) {
    ap_wifi_config.ap.authmode = WIFI_AUTH_OPEN;
  }

  ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));

  // Setup access point
  ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &ap_wifi_config));
  ESP_ERROR_CHECK(esp_wifi_start());

  // Print IP, MASK, GW info.
  tcpip_adapter_ip_info_t ip = { 0 };
  if (tcpip_adapter_get_ip_info(ESP_IF_WIFI_AP, &ip) == 0) {
    ESP_LOGI(esp_common__get_TAG(), "IP:"IPSTR, IP2STR(&ip.ip));
    ESP_LOGI(esp_common__get_TAG(), "MASK:"IPSTR, IP2STR(&ip.netmask));
    ESP_LOGI(esp_common__get_TAG(), "GW:"IPSTR, IP2STR(&ip.gw));
  }
}
